<?php
require_once("functions.php");

// Initiate session
session_name("tidal");
session_start();

$buttonOk = False;

// Initiate SQLite database connection
$db = new SQLite3('inc/tidal.db');

// Check if the lock file exists
$lock = file_exists(".tidal_lock");

// Check how many jobs the user can run, if not logged in it will use the IP
if ($_SESSION["logged_in"]) {
    $count = $db->querySingle("SELECT uses FROM accounts WHERE account_id=$_SESSION[account_id]");

    // If user has any credits left, unlock button
    if ($count > 0) {
        $buttonOk = true;

        if ($lock) {
            $buttonOk = False;
        }
    }
} else {
    $count = $db->querySingle("SELECT COUNT(1) as count FROM log WHERE ip=\"$_SERVER[REMOTE_ADDR]\" AND date LIKE \"".date('Y-m-d')."%\"");
        
    // If count is less than 5, unlock button
    if ($count < 5) {
        $buttonOk = True;

        if ($lock) {
            $buttonOk = False;
        }
    } 
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/cookie.notice.min.js"></script>

    <title>Tidal Media Web Downloader</title>

	<style>
	@media (max-width : 768px) {
		h3 {
			font-size: 1.5rem !important;
		}
		div#details {
			text-align: center !important;
		}
	}
	</style>

	<script>
	function setQualityCookie(name,days) {
		var expires = "";
		var value = document.getElementById("quality").value;
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days*24*60*60*1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	}
	function getQualityCookie(c_name) {
		if (document.cookie.length > 0) {
			c_start = document.cookie.indexOf(c_name + "=");
			if (c_start != -1) {
				c_start = c_start + c_name.length + 1;
				c_end = document.cookie.indexOf(";", c_start);
				if (c_end == -1) {
					c_end = document.cookie.length;
				}
				return unescape(document.cookie.substring(c_start, c_end));
			}
		}
		return ""; 
	}
	</script>

</head>
<body>
	<header class="p-3">
		<div class="container">
			<header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
			<h3 class="display-4">Tidal Media Downloader</h3>

			<div class="col-md-2 card" id="qualitySelector" style="padding:0.5em;">
						<label for="quality">Quality: 
							<a data-bs-toggle="collapse" href="#qualityInfoCollapse" aria-expanded="false" aria-controls="qualityInfoCollapse">
								(What does this mean?)
							</a>
						</label>
						<select class="form-select" aria-label="Quality selector" name="quality" id="quality" onchange="setQualityCookie('quality',7)">
							<option value="Master" selected>Master (MQA)</option>
							<option value="HiFi">HiFi</option>
							<option value="High">High</option>
							<option value="Normal">Normal</option>
							<option disabled>────── Experimental</option>
							<option value="Opus160k">Opus 160kbps</option>
							<option value="Opus128k">Opus 128kbps</option>
							<option value="Opus96k">Opus 96kbps</option>
							<option value="Opus64k">Opus 64kbps</option>
							<option disabled>────── Experimental</option>
							<option value="Lame320k">MP3 320kbps</option>
							<option value="Lame256k">MP3 256kbps</option>
							<option value="Lame192k">MP3 192kbps</option>
							<option value="Lame128k">MP3 128kbps</option>
						</select>
			</div>

			<div class="col-md-3 text-end" id="details">
			<?php
			if ($_SESSION["logged_in"]) {
				echo "<p>Logged in as  <code> ". join(" ", str_split($_SESSION['account_id'], 4)) . "</code><br>
				<span class=\"text-muted\">$count 
				<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" fill=\"currentColor\" alt=\"credits\" class=\"bi bi-ticket-perforated\" viewBox=\"0 0 20 20\">
					<path d=\"M4 4.85v.9h1v-.9H4Zm7 0v.9h1v-.9h-1Zm-7 1.8v.9h1v-.9H4Zm7 0v.9h1v-.9h-1Zm-7 1.8v.9h1v-.9H4Zm7 0v.9h1v-.9h-1Zm-7 1.8v.9h1v-.9H4Zm7 0v.9h1v-.9h-1Z\"/>
					<path d=\"M1.5 3A1.5 1.5 0 0 0 0 4.5V6a.5.5 0 0 0 .5.5 1.5 1.5 0 1 1 0 3 .5.5 0 0 0-.5.5v1.5A1.5 1.5 0 0 0 1.5 13h13a1.5 1.5 0 0 0 1.5-1.5V10a.5.5 0 0 0-.5-.5 1.5 1.5 0 0 1 0-3A.5.5 0 0 0 16 6V4.5A1.5 1.5 0 0 0 14.5 3h-13ZM1 4.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 .5.5v1.05a2.5 2.5 0 0 0 0 4.9v1.05a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-1.05a2.5 2.5 0 0 0 0-4.9V4.5Z\"/>
				</svg></span><br>
				<a href=\"logout.php\">Logout</a>
				</p>";
			} else {
				echo "<p>Not logged in<br>
				<span class=\"text-muted\">$count/5 jobs for today<br> <font size=\"0.5em\">(IP: $_SERVER[REMOTE_ADDR])</font></span><br>
				<a href=\"login.php\">Log in</a></p>";
			}
			?>
			</div>
    		</header>
  </div>
  </header>

	<main class="container">
			<p class="mb-2">Search a track name or an album.</p>
			<input type="text" class="form-control mb-3" id="id" name="id" placeholder="Type here..." onkeyup="setTimeout(showHint(this.value), 200);">


			<div class="cointainer" id="searchCard"></div>

			<?php
			if ($buttonOk == false) {
				echo "<div class=\"alert alert-danger\" role=\"alert\">
				Worker is currently serving another job. Please wait a moment until it finishes.<br>
				<strong>Starting a job while another one is running it will consume a credit and your job will not be queued.</strong>
			  </div>";
			}
			?>
			<br>
			<div class="collapse" id="qualityInfoCollapse">
				<h4>Quality selector information:</h4>
				<p>Quality selector allows you to choose from the four different streams Tidal has to offer.</p>
				<p>Tests were made with the song <b>Clocks</b> from <i>Coldplay</i> for comparison. More details about each one below:</p>
				<ul>
					<li><b>Master (MQA)</b>: Lossy codec that promises to deliver the best sound quality. FLAC container. FLAC audio codec. 932 kbps. 34.4 MiB. <a href="https://paste.delegao.moe/?16b6b66e17f7c1f9#7hVtnZ16RkrneozEb6MqS6muBc9w8SS4S566xY3iBtNi" target="_blank">Mediainfo</a></li>
					<li><b>HiFi</b>: True CD-quality stream in a FLAC container. FLAC audio codec. 932 kbps. 34.4 MiB. <a href="https://paste.delegao.moe/?2a6d90a652f88dae#AxC1QV7UnSmzvUJtMnNrcA1XqJ613SMkgWsQcTcLjPAj" target="_blank">Mediainfo</a></li>
					<li><b>High</b>: High quality stream in AAC-LC for wider DAP support. MP4 container. AAC-LC audio codec. 320 kbps. 12 MiB. <a href="https://paste.delegao.moe/?cfcef79302d00a47#6hapmSJcbweokCChHHCRKXg3R1avLibR76cUydgGRwRK" target="_blank">Mediainfo</a></li>
					<li><b>HiFi</b>: Standard quality for users with limited space. MP4 container. AAC-LC SBR audio codec. 96 kbps. 3.53 MiB. <a href="https://paste.delegao.moe/?f8e37e774ad88e3a#8uekNpgewLhyCfs1WA4bY9vxLqgdxcA473QHpMjsX28J" target="_blank">Mediainfo</a></li>
				</ul>

				<hr>

				<p>Experimental Opus and MP3 support has been added. These are transcoded from the HiFi (FLAC) files straight from Tidal after being downloaded.</p>
				<p>Opus is encoded using <i>opusenc (libopus 1.3.1)</i> and for MP3 we are using <i>ffmpeg's Lavc59.18.100 libmp3lame</i> encoder.</p>
			</div>
			
			<hr>
			<p>Files are wiped after two hours of being downloaded.</p>
			<p><a href="https://tidal.delegao.moe/download/">Downloads folder</a></p>
		</div>
	</main>

	<footer class="footer mt-auto py-3 bg-light border-top">
		<div class="container text-center">
			<p class="text-muted">Early access software. Please report any bugs in the <a href="https://codeberg.org/delegao/tidal-web-media-downloader" target="_blank">Codeberg repository</a>.</p>
			<p>v0.3.1 - <a href="changelog.html">Changelog</a></p>	
		</div>
	</footer>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script>
	// Update quality selector value
	document.getElementById("quality").value = getQualityCookie("quality");

	// Update search results
	function showHint(str) {
	var xhttp;
	if (str.length == 0) { 
		document.getElementById("searchCard").innerHTML = "Start looking for something...";
		return;
	}
	xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		document.getElementById("searchCard").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "search.php?q="+str, true);
	xhttp.send();   
	}
	</script>
</body>
</html>
