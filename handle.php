<?php
// Initiate session
session_name("tidal");
session_start();

require_once("functions.php");
header('X-Accel-Buffering: no');

// Initiate SQLite database connection
$db = new SQLite3('inc/tidal.db');


// Check if user provides an account ID to download
if (isset($_REQUEST["account"])) {
    $acc_id = htmlspecialchars($_REQUEST["account"]);

    $result = $db->querySingle("SELECT account_id FROM accounts WHERE account_id=$acc_id");

    if ($result) {
        $_SESSION["account_id"] = $acc_id;
        $_SESSION["logged_in"] = True;
    } else {
        echo("Unsuccessful login.");
        die();
    }
}


// Check how many jobs the user can run, if not logged in it will use the IP
if ($_SESSION["logged_in"]) {
    $count = $db->querySingle("SELECT uses FROM accounts WHERE account_id=$_SESSION[account_id]");
    
    // If user has no credits, do not continue
    if ($count <= 0) {
        http_response_code(403);
        die('You ran out of worker time. Please try again later.');
    } else {
		$db->exec("UPDATE accounts SET uses=uses-1, last_used=datetime() WHERE account_id = $_SESSION[account_id]"); // Remove one credit
	}
} else {
    $count = $db->querySingle("SELECT COUNT(1) as count FROM log WHERE ip=\"$_SERVER[REMOTE_ADDR]\" AND date LIKE \"".date('Y-m-d')."%\"");
            
    // If the IP has run 5 different jobs in the last 24 hours, do not continue
    if ($count >= 5) {
        http_response_code(403);
        die('You ran out of worker time. Please try again later.');
    } else {
		$db->exec("INSERT INTO log('ip') VALUES (\"$_SERVER[REMOTE_ADDR]\")");
	}
}
    

// Insert this job into the database
//$db->exec("INSERT INTO log('ip') VALUES (\"$_SERVER[REMOTE_ADDR]\")"); (We do not want to log logged-in users' IP addresses)

$qualityList = [
    "Master",
    "HiFi",
    "High",
    "Normal",
    "Opus160k",
    "Opus128k",
    "Opus96k",
    "Opus64k",
    "Lame320k",
    "Lame256k",
    "Lame192k",
    "Lame128k"
];

$typeList = [
    "album",
    "track"
];


$id = intval($_REQUEST['id']);
$quality = htmlspecialchars($_REQUEST['quality']);
$type = htmlspecialchars($_REQUEST['type']);

if($id < 1){ // Check if ID is an integer
    http_response_code(403);
    die("Forbidden");
} elseif((!in_array($type, $typeList))) { // Check if given parameters are valid
    http_response_code(403);
    die("Illegal request");
} elseif((!in_array($quality, $qualityList))) { // Check if given parameters are valid
    http_response_code(403);
    die("Illegal request");
} elseif(disk_free_space("/") < 1073741824) {
        echo("Worker ran out of space. Please try again later.");
        die();
} elseif(file_exists(".tidal_lock")) {
        echo("Worker busy. Please try again later.");
        die();
}

// Create the lock file
touch(".tidal_lock");

echo "<pre>";

// Check if the requested job comes direct from Tidal or requires transcoding
if ($quality == "Master" || $quality == "HiFi" || $quality == "High" || $quality == "Normal") {
    // Convert quality to tdl names
    switch ($quality) {
        case "Master":
            $quality = "max";
            break;
        case "HiFi":
            $quality = "lossless";
            break;
        case "High":
            $quality = "high";
            break;
        case "Normal":
            $quality = "low";
            break;
    }
    // Download directly from Tidal
    $result = liveExecuteCommand("TDL_DL_DIR=/var/www/tidal.delegao.moe/download timeout 120s /usr/local/bin/tdl get -p true -q $quality https://listen.tidal.com/$type/$id");
} elseif ($quality == "Opus128k" || $quality == "Opus96k" || $quality == "Opus64k") {

    // Convert quality variable to integer
    switch ($quality) {
        case "Opus160k":
            $quality = 160;
            break;
        case "Opus128k":
            $quality = 128;
            break;
        case "Opus96k":
            $quality = 96;
            break;
        case "Opus64k":
            $quality = 64;
            break;
    }
    // Download to the encoding folder and execute the transcoding script
    $result = liveExecuteCommand("TDL_DL_DIR=/var/www/tidal.delegao.moe/encode timeout 120s /usr/local/bin/tdl get -p true -q max https://listen.tidal.com/$type/$id && /var/www/tidal.delegao.moe/inc/encode.sh opus $quality");
} else {

    // Convert quality variable to integer
    switch ($quality) {
        case "Lame320k":
            $quality = 320;
            break;
        case "Lame256k":
            $quality = 256;
            break;
        case "Lame192k":
            $quality = 192;
            break;
        case "Lame128k":
            $quality = 128;
            break;
    }
    // Download to the encoding folder and execute the transcoding script
    $result = liveExecuteCommand("TDL_DL_DIR=/var/www/tidal.delegao.moe/encode timeout 120s /usr/local/bin/tdl get -p true -q max https://listen.tidal.com/$type/$id && /var/www/tidal.delegao.moe/inc/encode.sh lame $quality");
}
echo "</pre>";

echo "<p>Downloads directory: <a href=\"https://tidal.delegao.moe/download/\">https://tidal.delegao.moe/download/</a><br>";
echo "<a href=\"https://tidal.delegao.moe/\">< Go back</a></p>";

unlink(".tidal_lock");
