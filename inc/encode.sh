#!/bin/bash
# Variables
encode_dir=/var/www/tidal.delegao.moe/encode
target_dir=/var/www/tidal.delegao.moe/download

# Create the array with the files to transcode
readarray -d '' array < <(find $encode_dir -type f -name "*.flac" -print0)

for entry in "${array[@]}"
do

	# Define input and output, delete trailing spaces and add double quotes
	input=$entry
	#output=${input/flac/opus}
	echo $input

	# Check if first argument is opus or lame, and transcode accordingly
	if [ $1 = "opus" ]
	then
		output=${input/flac/opus}
		timeout 10s ffmpeg -y -i "$input" -vf scale=400:400 -loglevel quiet -stats cover.jpg
		timeout 30s opusenc --bitrate $2 --discard-pictures --picture cover.jpg "$input" "$output"
		#timeout ffmpeg -i "$input" -c:a libopus -b:a 128k -id3v2_version 3 Survive5.opus
	else
		output=${input/flac/mp3}
		timeout 30s ffmpeg -i "$input" -map 0:a:0 -map 0:v:0 -filter:v scale=w=400:h=400,format=yuvj420p -c:v mjpeg -c:a libmp3lame -b:a $2k -f mp3 -id3v2_version 3 -loglevel quiet -stats "$output"
	fi

	# Delete input file
	rm "$input"
done

# When finished, move everything over to the downloads folder
cp -r "$encode_dir" "$target_dir"
rm -rf "$encode_dir"/*
