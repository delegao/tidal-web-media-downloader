<?php
// Initiate session
session_name("tidal");
session_start();

$error = false;

// Initiate SQLite database connection
$db = new SQLite3('inc/tidal.db');

// Check if the user is logged in
if ($_SESSION["logged_in"] == True) {
    header('Location: index.php');
    die();
} else {
    // If not, process the submitted form
    if (isset($_REQUEST["account_id"])) {
        $acc_id = htmlspecialchars($_REQUEST["account_id"]);

        $result = $db->querySingle("SELECT account_id FROM accounts WHERE account_id=$acc_id");

        if ($result) {
            $_SESSION["account_id"] = $acc_id;
            $_SESSION["logged_in"] = True;

            header('Location: index.php');
            die();
        } else {
            $error = True;
        }
    }
}
    ?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Login - Tidal Media Web Downloader</title>
    <style>
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }
    </style>
</head>
<body>
    <main class="form-signin">

        <h1 class="h5 mb-3 fw-normal">Please enter your account ID</h1>
        <form method="POST" action="">
            <div class="form-floating mb-1">
                <input class="form-control" id="account_id" name="account_id" type="tel" inputmode="numeric" pattern="[0-9\s]{13,19}" autocomplete="cc-number" maxlength="19" placeholder="1234 5678 9123 4567" required="">
                <label for="floatingInput">Account ID</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>

            <?php
                if ($error) {
                    echo "<div class=\"alert alert-danger mt-3\" role=\"alert\">
                        <strong>Login failed.</strong> Check your details.
                    </div>";
                }
            ?>
        </form>

    </main>
</body>
</html>