<?php
// Initiate session
session_name("tidal");
session_start();

session_destroy();

header('Location: index.php');
die();